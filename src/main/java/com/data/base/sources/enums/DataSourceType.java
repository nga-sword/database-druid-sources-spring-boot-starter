package com.data.base.sources.enums;

/**
 * @author: create by adoreFT
 * @version: v1.0
 * @keyWord: Time is not waiting for me, you forgot to take me away.
 * @description: DataSourceType  多数据源
 * @Date: 03/03/22 11:13 AM
 **/
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE

}
