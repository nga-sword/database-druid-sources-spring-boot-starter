package com.data.base.sources.annotation;


import com.data.base.sources.enums.DataSourceType;

import java.lang.annotation.*;

/**
 * 自定义多数据源切换注解
 * <p>
 * 优先级：先方法，后类，如果方法覆盖了类上的数据源类型，以方法的为准，否则以类上的为准
 *
 * @author: create by adoreFT
 * @version: v1.0
 * @keyWord: Time is not waiting for me, you forgot to take me away.
 * @description: DataSource
 * @Date: 03/03/22 11:13 AM
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DataSource {
    /**
     * 切换数据源名称
     */
    public DataSourceType value() default DataSourceType.MASTER;
}
